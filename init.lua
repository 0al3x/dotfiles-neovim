require('plugin')

-----------------
-- lsp section --
-----------------
local capabilities = require("cmp_nvim_lsp").default_capabilities()

local rust_opts = {
  tools = { 
    executor = require("rust-tools.executors").termopen,
    on_initialized = nil,
    reload_workspace_from_cargo_toml = true,
    inlay_hints = {
      auto = true,
      only_current_line = false,
      show_parameter_hints = true,
      parameter_hints_prefix = "<- ",
      other_hints_prefix = "=> ",
      max_len_align = false,
      max_len_align_padding = 1,
      right_align = false,
      right_align_padding = 7,
      highlight = "Comment",
    },

    hover_actions = {
      border = {
        { "╭", "FloatBorder" },
        { "─", "FloatBorder" },
        { "╮", "FloatBorder" },
        { "│", "FloatBorder" },
        { "╯", "FloatBorder" },
        { "─", "FloatBorder" },
        { "╰", "FloatBorder" },
        { "│", "FloatBorder" },
      },
      max_width = nil,
      max_height = nil,
      auto_focus = false,
    },
    crate_graph = {
      backend = "wayland",
      output = nil,
      full = true,

      enabled_graphviz_backends = {
        "bmp",
        "cgimage",
        "canon",
        "dot",
        "gv",
        "xdot",
        "xdot1.2",
        "xdot1.4",
        "eps",
        "exr",
        "fig",
        "gd",
        "gd2",
        "gif",
        "gtk",
        "ico",
        "cmap",
        "ismap",
        "imap",
        "cmapx",
        "imap_np",
        "cmapx_np",
        "jpg",
        "jpeg",
        "jpe",
        "jp2",
        "json",
        "json0",
        "dot_json",
        "xdot_json",
        "pdf",
        "pic",
        "pct",
        "pict",
        "plain",
        "plain-ext",
        "png",
        "pov",
        "ps",
        "ps2",
        "psd",
        "sgi",
        "svg",
        "svgz",
        "tga",
        "tiff",
        "tif",
        "tk",
        "vml",
        "vmlz",
        "wbmp",
        "webp",
        "xlib",
        "x11",
      },
    },
  },

  server = {
    {
	    capabilities = capabilities,
      ['rust-analyzer'] = {
        filetypes = {"rust", "yew"},
        diagnostics = {
          enable = true,
        },
        annotations = true,
        cargo = {
	        features = "all",
	      },
        procMacro = {
          enable = true,
        }
      },
    }
  }, -- rust-analyzer options

  -- debugging stuff
  --dap = {
  --  adapter = {
  --    type = "executable",
  --    command = "lldb-vscode",
  --    name = "rt_lldb",
  --  },
  --},
}

require('rust-tools').setup(rust_opts)

-------------
-- haskell --
-------------

require('lspconfig').hls.setup{
  filetypes = { 'haskell', 'lhaskell', 'cabal' },
}

------------
-- python --
------------

require'lspconfig'.pyright.setup{}

---------------
-- cssls lsp and html --
---------------
local capabilities = require('cmp_nvim_lsp').default_capabilities()

capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.html.setup {
  capabilities = capabilities,
  filetypes = { "html", "yew" },
  init_options = {
    configurationSection = { "html" },
    embeddedLanguages = {
      css = true,
      javascript = false
    },
    provideFormatter = true
  }
}

capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = {
    "documentation",
    "detail",
    "additionalTextEdits",
  },
}

require('lspconfig').cssls.setup {
  capabilities = capabilities,
}

-----------------
-- tree sitter --
-----------------

require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "c", "cpp", "lua", "vim", "vimdoc", "rust", "html", "haskell", "ocaml", "python" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  highlight = {
    enable = true,
  },
}

-----------------
-- set folding --
-----------------

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

vim.api.nvim_create_autocmd( { "BufReadPost", "FileReadPost", "BufEnter" }, {
  pattern = { "*" },
  command = "normal zR",
})

vim.cmd([[
highlight Folded guibg=none guifg=#2aa198
highlight LineNr guifg=#d33682
set fillchars=fold:\ "\

set foldtext=MyFoldText()

function MyFoldText()
  let line = getline(v:foldstart)
  let sub = substitute(line, '/\*\|\*/\|{{{\d\=', '', 'g')
  return v:folddashes . sub . ' ... }'
endfunction
]])

require('Comment').setup {
  toggler = {
    line = 'cc',
    block = 'bc',
  },
  opleader = {
    line = 'cc',
    block = 'bc',
  },
}

require('hop').setup()
require('telescope').setup()
require('crates').setup()

require('global')
require('keymap')

