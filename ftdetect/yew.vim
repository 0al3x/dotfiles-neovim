"---- format on save
"vim.api.nvim_create_autocmd( { "BufNewFile", "BufRead" }, {
"  pattern = { "*_yew.rs" },
"  command = "set filetype yew",
"})

au BufRead,BufNewFile *_yew.rs set filetype=yew
