-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]


return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- theme
  use 'overcache/NeoSolarized'

  -- neovim lsp
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp

  -- completion
  use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
  --use 'hrsh7th/cmp-buffer'
  --use 'hrsh7th/cmp-path'
  use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp

  -- snippets
  use 'L3MON4D3/LuaSnip' -- Snippets plugin

  -- nvim tree
  use 'nvim-tree/nvim-tree.lua'
  use 'nvim-tree/nvim-web-devicons'

  -- rust file detection, .. etc
  use 'rust-lang/rust.vim'

  -- treesitter
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
  }

  -- nvim presnce
  use 'andweeb/presence.nvim'

  -- nvim bar
  use "sitiom/nvim-numbertoggle"

  use 'simrat39/rust-tools.nvim'

  -- hop
  use 'phaazon/hop.nvim'

  -- telescope
  use {
    'nvim-telescope/telescope.nvim', branch = '0.1.x',
  }

  -- crates
  use {
      'saecki/crates.nvim',
      tag = 'v0.3.0',
      requires = { 'nvim-lua/plenary.nvim' },
      config = function()
        require('crates').setup()
      end,
  }

  use "numToStr/Comment.nvim"

end)
