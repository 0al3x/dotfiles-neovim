-- diagnostics
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float)
vim.keymap.set('n', '<leader>d', vim.diagnostic.goto_prev)
vim.keymap.set('n', '<leader>', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- save changes
vim.keymap.set('n', '<C-s>', ':w!<cr>')

-- create tabs
vim.keymap.set('n', '<C-t>', ':tabnew<cr>')

-- close tabs
vim.keymap.set('n', 'qq', ':q<cr>')

-- splits
vim.keymap.set('n', '<M-Right>', ':vs !<cr>')
vim.keymap.set('n', '<M-Left>', ':vs !<CR><C-W><C-R>')
vim.keymap.set('n', '<M-Up>', ':sp !<CR><C-W><C-R>')
vim.keymap.set('n', '<M-Down>', ':sp !<cr>')

--spliting clean
vim.keymap.set('n', '<C-S-Right>', ':vs<cr>')
vim.keymap.set('n', '<C-S-Left>', ':vs<CR><C-W><C-R>')
vim.keymap.set('n', '<C-S-Up>', ':sp<CR><C-W><C-R>')
vim.keymap.set('n', '<C-S-Down>', ':sp<cr>')

--reverse splits
vim.keymap.set('n', '<M-Space>', '<C-W><C-R>')

--tabmove
vim.keymap.set('n', '<M-PageUp>', ':tabmove -<cr>')
vim.keymap.set('n', '<M-PageDown>' , ':tabmove +<cr>')

--tabnew
vim.keymap.set('n', '<C-t>' , ':tabnew<cr>')

--resize splitts
vim.keymap.set('n', '<S-L>' , ':vertical resize +2<cr>')
vim.keymap.set('n', '<S-H>' , ':vertical resize -2<cr>')
vim.keymap.set('n', '<S-K>' , ':resize +2<cr>')
vim.keymap.set('n', '<S-J>' , ':resize -2<cr>')

--tab close
vim.keymap.set('n', '<M-Esc>' , ':q<cr>')

--open terminal here
vim.keymap.set('n', '<S-t>' , ':ter!<cr>')

--why redo does not works?
vim.keymap.set('n', '<S-u>' , ':redo<cr>')

-- key moving into splits
vim.keymap.set('n', '<M-NL>', '<C-W><C-J>')
vim.keymap.set('n', '<M-C-K>', '<C-W><C-K>')
vim.keymap.set('n', '<M-C-L>', '<C-W><C-L>')
vim.keymap.set('n', '<M-C-H>', '<C-W><C-H>')
vim.keymap.set('n', '<BS>', '<C-W><C-H>')

vim.keymap.set('n', '<M-C-Down>', '<C-W><C-J>')
vim.keymap.set('n', '<M-C-Up>', '<C-W><C-K>')
vim.keymap.set('n', '<M-C-Right>', '<C-W><C-L>')
vim.keymap.set('n', '<M-C-Left>', '<C-W><C-H>')

--clean highlighting search
vim.keymap.set('n', '<F2>' , ':noh<cr>')

-- nvim tree
vim.keymap.set('n', '<space>t', ':NvimTreeToggle<cr>')
vim.keymap.set('n', '<space>tt', ':NvimTreeFocus<cr>')
vim.keymap.set('n', '<space>c', ':NvimTreeColapse<cr>')


-- reset tab behaviour
vim.keymap.set('i', '<Tab>', '\t')
