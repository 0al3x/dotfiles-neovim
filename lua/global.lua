-- syntax highlingth
vim.cmd.syntax("enable")
vim.cmd.filetype({"plugin", "indent", "on"})

-- general editor configs
vim.opt.number = true
vim.opt.cursorline = true
vim.opt.colorcolumn = "100"
vim.opt.title = true
vim.opt.mouse = "a"
vim.opt.ignorecase = true
vim.opt.smartcase = true 
vim.opt.wrap = true

-- 'soft' split opening
vim.opt.splitbelow = true
vim.opt.splitright = true

-- tabs settings
vim.opt.autoindent = true
vim.opt.expandtab = false
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.cmd[[set noexpandtab]]
-- vim.opt.softtabstop = 4
-- vim.opt.shiftround = true



-- theme
vim.g.solarized_termcolors=256
vim.opt.termguicolors = true
vim.cmd.colorscheme("NeoSolarized")

-- gui config
vim.opt.guifont = { "Hack", ":h11" }
vim.g.neovide_cursor_animation_length = 0
vim.g.neovide_cursor_trail_size = 0

---------------
-- nvim tree --
---------------

-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- OR setup with some options
require("nvim-tree").setup({
  update_focused_file = {
    enable = true,
  },
  sort_by = "case_sensitive",
  view = {
    width = 30,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
})
